/**
 * Created by niklaskappler on 28.06.17.
 */

function hexClock() {

    var newDate = new Date(),
        day = newDate.getDay(),
        hours = newDate.getHours(),
        minutes = newDate.getMinutes().toString(),
        seconds = newDate.getSeconds().toString();
    var string = '';

    /*if (hours > 12 && hours !== 0) {
        hours = hours - 12;
    }*/
    if (minutes < 10) {
        minutes = 0 + minutes;
    }
    if (seconds < 10) {
        seconds = 0 + seconds;
    }
    var minsSecs = minutes*60;

    string = (hours<=9)? '0'+hours : hours;
    string = '#'+string+minutes+seconds;
    $('.time').html(string);
    $('body').css("background-color",string)

}

setInterval(function() {
    hexClock();
}, 1000);







